#include <sqlite3.h>
#include <syslog.h>
#include <time.h>

#include "db.h"

// call once upon init
int init_mqtt_sub_db() {

    char *err_msg = 0;

    sqlite3 *db;

    // initialize
    int rc = sqlite3_open(LOG_DB_PATH, &db);
    if (rc != SQLITE_OK) {
        syslog(LOG_ERR, "Cannot open database: %s", sqlite3_errmsg(db));
        sqlite3_close(db);
        return -1;
    }

    // create table for db if it doesn't exist
    const char *init_sql =
        "CREATE TABLE IF NOT EXISTS messages ("
        "mid INTEGER PRIMARY KEY,"
        "timestamp DATETIME NOT NULL,"
        "qos INTEGER NOT NULL,"
        "topic TEXT NOT NULL,"
        "message TEXT NOT NULL"
        ");";

    rc = sqlite3_exec(db, init_sql, 0, 0, &err_msg);

    if (rc != SQLITE_OK) {
        syslog(LOG_ERR, "SQL error on initial query: %s", err_msg);
        sqlite3_free(err_msg);
        sqlite3_close(db);
        return 1;
    }

    // close db, open later
    sqlite3_free(err_msg);
    sqlite3_close(db);
    return 0;
}

int log_mqtt_message(const struct mosquitto_message *msg) {
    int rc;
    unsigned int timestamp = time(NULL); // current unix time
    char *err_msg;

    sqlite3 *db;

    // open
    rc = sqlite3_open(LOG_DB_PATH, &db);
    if (rc != SQLITE_OK) {
        syslog(LOG_ERR, "Cannot open database: %s", sqlite3_errmsg(db));
        sqlite3_close(db);
        return -1;
    }


    char sql[512] = {'\0'};
    sqlite3_snprintf(sizeof(sql), sql,
        "INSERT INTO messages (timestamp, qos, topic, message) VALUES ('%u', '%u', '%s', '%s');",
        timestamp,
        msg->qos,
        msg->topic,
        (char *)msg->payload
    );

    rc = sqlite3_exec(db, sql, 0, 0, &err_msg);
    if (rc != SQLITE_OK) {
        sqlite3_free(err_msg);
        syslog(LOG_DEBUG, "SQL error while writing message: %s",sqlite3_errmsg(db));
    }

    sqlite3_close(db); // move this out to separate call if closing sequence is more than one line

    return 0;
}
