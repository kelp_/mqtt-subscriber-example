#ifndef CONFIG_H
#define CONFIG_H

#define MQTT_MAX_TOPIC_LEN 65536 // in bytes
#define MQTT_MAX_TOPIC_COUNT 10
// 10 * 65536 bytes => about half a megabyte. dynalloc instead?

#define MQTT_MAX_EVENT_LISTENER_COUNT 10

// https://stackoverflow.com/questions/417142/what-is-the-maximum-length-of-a-url-in-different-browsers
#define MAX_URL_SIZE 2048

enum topic_event_value_type {
    EV_VALUE_TYPE_UNSET,
    EV_VALUE_TYPE_DECIMAL,
    EV_VALUE_TYPE_STRING
};

enum topic_event_comparison_type {
    EV_COMP_TYPE_UNSET,
    EV_COMP_TYPE_EQUAL,
    EV_COMP_TYPE_NOT_EQUAL,
    EV_COMP_TYPE_GREATER,
    EV_COMP_TYPE_LESSER,
    EV_COMP_TYPE_GREATER_EQUAL,
    EV_COMP_TYPE_LESSER_EQUAL
};

// email
struct event_email_group {
    char *name;
    char *sender_email;
    char *smtp_ip;
    int smtp_port;
    int secure_connection;
    int use_credentials;
    char *username;
    char *password;
};

// main config struct.
// maps to uci
// contains an array of topics and an array of event listeners
struct mqtt_sub_config {
    int enable;
    // not screwing around with mosq TLS because that seems like a can of worms for another day
    //int useTLS;
    char url[MAX_URL_SIZE];
    int port;
    struct mqtt_conf_topic *topics[MQTT_MAX_TOPIC_COUNT];
    struct mqtt_topic_event *events[MQTT_MAX_EVENT_LISTENER_COUNT];
    struct event_email_group emailgroup;
};

// mqtt topic struct.
// mostly here for future expandability (could add QoS as int)
struct mqtt_conf_topic {
    char name[MQTT_MAX_TOPIC_LEN];
};

// mqtt event listener struct
// TODO: move str_val and int_val into a union.
struct mqtt_topic_event {
    char topic_name[MQTT_MAX_TOPIC_LEN];
    char json_key[256];
    enum topic_event_value_type value_type;
    enum topic_event_comparison_type comp_type;
    char str_val[512];
    char comment[512];
    int int_val; // TODO: change this to floating point
    char *recipient_email;
};

struct mqtt_sub_config read_mqtt_sub_config();

struct mqtt_sub_config alloc_mqtt_sub_config();

void free_mqtt_sub_config(struct mqtt_sub_config conf);

#endif /* CONFIG_H */