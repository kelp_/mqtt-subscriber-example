#include <stddef.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <curl/curl.h>
#include <sys/syslog.h>
#include <uuid/uuid.h>
#include <syslog.h>
#include <unistd.h>

#include "config.h"

#define FMEM_BUFSIZ 1024

// generates a string uuid
char *generate_char_uuid() {
    uuid_t binuuid;
    uuid_generate_random(binuuid);
    char *uuid = calloc(FMEM_BUFSIZ, sizeof(char));
    uuid_unparse_lower(binuuid, uuid);
    return uuid;
}

// copied from example in libcurl's docs
static size_t payload_source(char *ptr, size_t size, size_t nmemb, void *userp) {

    FILE *readhere = (FILE *)userp;
    curl_off_t nread;
    char *buf = calloc(FMEM_BUFSIZ, sizeof(char));

    size_t retcode = fread(buf, size, nmemb, readhere);

    size_t len = strnlen(buf, FMEM_BUFSIZ);
    memcpy(ptr, buf, len);

    free(buf);

    if (retcode == 0) { // if we didn't read anything (0 bytes from fread)
        fclose(readhere);// close the file we had opened prior
        return retcode;// return 0 to indicate end of payload for libcurl
    }

    return len;// if we read something, use the strnlen size_t
}

FILE *generate_tmp_file_payload(const char *value, struct event_email_group *emailgroup, struct mqtt_topic_event *event) {

    // seems to be problems with reading/writing to files in fs.
    // dont know what's up.
    //FILE *fp = fopen(TMP_MAIL_FILE, "r");

    // since it doesn't matter for our usecase,
    // we can use an in-memory file for this.
    FILE *fp = fmemopen(NULL, FMEM_BUFSIZ, "r+");

    if (!fp) { // file err chk
        syslog(LOG_ERR, "Unable to open tmpfile. aborting.");
        return NULL;
    }

    char buffer[1024];
    size_t bufsiz = sizeof(buffer);

    // write subject
    fprintf(fp, "Subject: [mqtt_test_sub] Event triggered.\r\n");

    // write sender and recipient into file
    fprintf(fp, "From: <%s>\r\nTo: <%s>\r\n", emailgroup->sender_email, event->recipient_email);

    // write message id into file
    snprintf(buffer, bufsiz, "%s", generate_char_uuid());
    fprintf(fp, "Message-ID: <%s>\r\n", buffer);

    // write date into file
    time_t rawtime;
    struct tm *info;
    time(&rawtime);
    info = localtime(&rawtime);

    strftime(buffer, bufsiz, "%a, %d %b %Y %T %z (%Z)\r\n", info);

    fprintf(fp, "Date: %s\r\n\r\n", buffer);

    // write content
    fprintf(fp, "Event with comment '%s' triggered.\r\n", event->comment); // TODO: allow name specification in event handlers.
    fprintf(fp, "Topic: %s\r\n", event->topic_name); // TODO: impl comp_type itoa
    fprintf(fp, "Set trigger value: '%s',", value);

    if (event->value_type == EV_VALUE_TYPE_DECIMAL) {
        fprintf(fp, " got: '%d'\r\n", event->int_val);
    } else if (event->value_type == EV_VALUE_TYPE_STRING) {
        fprintf(fp, " got: '%s'\r\n", event->str_val);
    }

    // flush, reset and return fp
    fflush(fp);
    rewind(fp);
    return fp;
}

// returns:
// 0 if successful send
// -1 if libcurl error
// -2 if missing email addresses
// -3 if missing smtp address or port
// -4 if use_credentials set, but no user/pwd provided
// -5 if failed to generate message
int send_smtp_message(const char *value, struct event_email_group *emailgroup, struct mqtt_topic_event *event) {
    CURL *curl;
    CURLcode res = CURLE_OK;
    struct curl_slist *recipients = NULL;
  
    // address fill check
    if (strcmp(event->recipient_email, "(null)") == 0 || strcmp(emailgroup->sender_email, "(null)") == 0) {
        return -2;
    }

    // smtp check
    if (emailgroup->smtp_port == 0 || strcmp(emailgroup->smtp_ip, "(null)") == 0) {
        return -3;
    }

    // user info check
    if (emailgroup->use_credentials == 1) {
        if (strcmp(emailgroup->username, "(null)") == 0 || strcmp(emailgroup->password, "(null)") == 0) {
            return -4;
        }
    }

    // generate payload text file
    FILE *payload = generate_tmp_file_payload(value, emailgroup, event);

    if (!payload) { // err chk
        return -5;
    }

    curl = curl_easy_init();
    if (!curl) {
        return -1;
    }

    // SMTP RELATED
    char buf[512];
    snprintf(buf, 512, "smtp://%s:%d", emailgroup->smtp_ip, emailgroup->smtp_port);
    printf("url: %s\n", buf);
    curl_easy_setopt(curl, CURLOPT_URL, buf);

    curl_easy_setopt(curl, CURLOPT_MAIL_FROM, emailgroup->sender_email);

    recipients = curl_slist_append(recipients, event->recipient_email);
    curl_easy_setopt(curl, CURLOPT_MAIL_RCPT, recipients);

    // TLS RELATED
    if (emailgroup->secure_connection == 1) {

        // AUTH
        if (emailgroup->use_credentials == 1) {
            curl_easy_setopt(curl, CURLOPT_USERNAME, emailgroup->username);
            curl_easy_setopt(curl, CURLOPT_PASSWORD, emailgroup->password);
        }

        curl_easy_setopt(curl, CURLOPT_USE_SSL, (long)CURLUSESSL_ALL);
        curl_easy_setopt(curl, CURLOPT_CAINFO, "/etc/cacert.pem");
    }

    // DATA
    curl_easy_setopt(curl, CURLOPT_READFUNCTION, payload_source);
    curl_easy_setopt(curl, CURLOPT_READDATA, (void *)payload);
    curl_easy_setopt(curl, CURLOPT_UPLOAD, 1L);
    
    // DEBUG
    //curl_easy_setopt(curl, CURLOPT_VERBOSE, 1L);

    // Send the message
    res = curl_easy_perform(curl);

    // Check for errors 
    if(res != CURLE_OK) {
        syslog(LOG_ERR, "curl_easy_perform() failed: rc: %d, str: %s\n",
        res, curl_easy_strerror(res));
    }

    curl_slist_free_all(recipients);

    curl_easy_cleanup(curl);

    return 0;
}