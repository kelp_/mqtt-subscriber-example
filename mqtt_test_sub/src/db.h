#ifndef DB_H
#define DB_H

#include <mosquitto.h>

#define LOG_DB_PATH "/log/mqtt_sub.db"

int init_mqtt_sub_db();

int log_mqtt_message(const struct mosquitto_message *msg);

#endif /* DB_H */