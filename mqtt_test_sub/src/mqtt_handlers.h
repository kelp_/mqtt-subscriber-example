#ifndef MQTT_HANDLERS_H
#define MQTT_HANDLERS_H

#include <mosquitto.h>
#include "config.h"

// wrapper around some mosquitto initialization functions, to keep main() clean.
void mosq_full_init(struct mosquitto *mosq, struct mqtt_sub_config conf);

// callback on MQTT connection
void cb_mqtt_conn(struct mosquitto *mosq, void *obj, int reason_code);

// callback on MQTT subscription
void cb_mqtt_sub(struct mosquitto *mosq, void *obj, int mid, int qos_count, const int *granted_qos);

// callback on MQTT message
void cb_mqtt_msg(struct mosquitto *mosq, void *obj, const struct mosquitto_message *msg);

#endif /* MQTT_HANDLERS_H */