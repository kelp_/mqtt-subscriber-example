#include <stdio.h>
#include <stdlib.h>
#include <mosquitto.h>
#include <syslog.h>
#include <string.h>
#include <time.h>
#include <json-c/json.h>

#include "config.h"
#include "mqtt_handlers.h"
#include "event.h"
#include "db.h"

void mosq_full_init(struct mosquitto *mosq, struct mqtt_sub_config conf) {
    int rc;
    
    // mosq lib init
    mosquitto_lib_init();

    // mosq struct init,
    // pointer of conf gets passed down as void *obj
    mosq = mosquitto_new(NULL, true, &conf);

    if(mosq == NULL){
        printf("Out of memory!\n");
        syslog(LOG_CRIT, "Out of memory!");
        exit(1);
    }
    

    mosquitto_connect_callback_set(mosq, cb_mqtt_conn);
    mosquitto_subscribe_callback_set(mosq, cb_mqtt_sub);
    mosquitto_message_callback_set(mosq, cb_mqtt_msg);

    rc = mosquitto_connect(mosq, conf.url, conf.port, 60);
    if(rc != MOSQ_ERR_SUCCESS){
        mosquitto_destroy(mosq);
        syslog(LOG_ERR, "Mosquitto error: %s\n", mosquitto_strerror(rc));
        exit(1);
    }

    // start mosq loop, will block until SIGINT/error
    mosquitto_loop_forever(mosq, -1, 1);
}

void cb_mqtt_conn(struct mosquitto *mosq, void *obj, int reason_code) {
    int rc;
    /* Print out the connection result. mosquitto_connack_string() produces an
     * appropriate string for MQTT v3.x clients, the equivalent for MQTT v5.0
     * clients is mosquitto_reason_string().
     */
    syslog(LOG_INFO, "on_connect: %s", mosquitto_connack_string(reason_code));
    if(reason_code != 0){
        syslog(LOG_ERR, "Mosquitto connection failed.");
        mosquitto_disconnect(mosq);
    }
    struct mqtt_sub_config *conf = (struct mqtt_sub_config *)obj;
    for (int i = 0; i < MQTT_MAX_TOPIC_COUNT; i++) {
        if (strcmp(conf->topics[i]->name, "") != 0) {
            syslog(LOG_INFO, "Subscribing to %s", conf->topics[i]->name);
            rc = mosquitto_subscribe(mosq, NULL, conf->topics[i]->name, 1);
            if(rc != MOSQ_ERR_SUCCESS){
                syslog(LOG_ERR, "Mosquitto error when subscribing: %s", mosquitto_strerror(rc));
            }
        }
    }
}

void cb_mqtt_sub(struct mosquitto *mosq, void *obj, int mid, int qos_count, const int *granted_qos) {
    int i;
    bool have_subscription = false;

    /* In this example we only subscribe to a single topic at once, but a
     * SUBSCRIBE can contain many topics at once, so this is one way to check
     * them all. */
    for(i=0; i<qos_count; i++){
        if(granted_qos[i] <= 2){
            have_subscription = true;
        }
    }
    if(have_subscription == false){
        syslog(LOG_ERR, "Mosquitto error: rejected.");
    }
}

void cb_mqtt_msg(struct mosquitto *mosq, void *obj, const struct mosquitto_message *msg) {
    log_mqtt_message(msg);

    struct mqtt_sub_config *conf = (struct mqtt_sub_config *)obj;

    handle_event_listeners(msg, conf);
}