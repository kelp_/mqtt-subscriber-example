#ifndef EVENT_H
#define EVENT_H

#include <json-c/json.h>
#include <mosquitto.h>

#include "config.h"

// parses message, loops over listeners in conf, sends smtp message if match
void handle_event_listeners(const struct mosquitto_message *msg, struct mqtt_sub_config *conf);

// tries to parse an incoming mosq message
// return codes:
// 0 if successful, *val has the right info in it
// -1 if failed to parse json
// -2 if failed to find matching json key
// -3 if disallowed type
static int event_try_parse(const struct mosquitto_message *msg, char *key, const char **output);


#endif /* EVENT_H */