#include <stdio.h>
#include <signal.h>
#include <sys/syslog.h>
#include <syslog.h>
#include <mosquitto.h>
#include <stdlib.h>

#include "config.h"
#include "mqtt_handlers.h"
#include "db.h"

volatile sig_atomic_t deamonize = 1;

int main() {

    struct mosquitto *mosq;
    int rc;

    // init log
    openlog("mqtt_test_sub", LOG_INFO, 0);
    syslog(LOG_INFO, "Initializing");

    // read conf from uci
    struct mqtt_sub_config conf = read_mqtt_sub_config();

    if (!conf.enable) { // no point in running further if conf says that we shouldn't be running.
        syslog(LOG_INFO, "Daemon disabled in config. Stopping.");
        printf("Daemon not enabled: stopping.\n");
        return 0;
    }

    // init db
    init_mqtt_sub_db();

    // init mosquitto (hangs until sigint)
    mosq_full_init(mosq, conf);

    // cleanup
    mosquitto_destroy(mosq);
    mosquitto_lib_cleanup();
    free_mqtt_sub_config(conf);
    closelog();
    return 0;
}
