#include <mosquitto.h>
#include <json-c/json.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <syslog.h>
#include <curl/curl.h>

#include "event.h"
#include "config.h"
#include "mail.h"

// handle decimal type comparisons (1 on true, 0 on false, neg if err) (TODO: conv to float/double)
int handle_comparison_decimal(int value, struct mqtt_topic_event *event) {
    enum topic_event_comparison_type ct = event->comp_type;
    int cmp_value = event->int_val;
    int rc;

    // using ternary operators to save space
    // https://www.freecodecamp.org/news/c-ternary-operator/
    switch(ct) {
        case EV_COMP_TYPE_EQUAL:
            rc = (cmp_value == value) ? 1 : 0;
            return rc;
        case EV_COMP_TYPE_NOT_EQUAL:
            rc = (cmp_value != value) ? 1 : 0;
            return rc;
        case EV_COMP_TYPE_GREATER:
            rc = (cmp_value > value) ? 1 : 0;
            return rc;
        case EV_COMP_TYPE_LESSER:
            rc = (cmp_value < value) ? 1 : 0;
            return rc;
        case EV_COMP_TYPE_GREATER_EQUAL:
            rc = (cmp_value >= value) ? 1 : 0;
            return rc;
        case EV_COMP_TYPE_LESSER_EQUAL:
            rc = (cmp_value <= value) ? 1 : 0;
            return rc;
        default:
            return -1;
    }
}

// handle string comparisons (1 on true, 0 on false, neg if err)
int handle_comparison_string(const char *value, struct mqtt_topic_event *event) {
    enum topic_event_comparison_type ct = event->comp_type;
    char *cmp_value = event->str_val;
    int rc;

    switch(ct) {
        case EV_COMP_TYPE_EQUAL:
            rc = (strcmp(value, cmp_value) == 0) ? 1 : 0;
            return rc;
        case EV_COMP_TYPE_NOT_EQUAL:
            rc = (strcmp(value, cmp_value) != 0) ? 1 : 0;
            return rc;
        default:// other comparisons are unsupported in strings
            return -1;
    }
}

// returns:
// 1 if event should be triggered
// 0 if event should not be triggered
// -1 if event is misvalued
int handle_comparison(const char *value, struct mqtt_topic_event *event) {
    if (event->value_type == EV_VALUE_TYPE_STRING) {
        return handle_comparison_string(value, event);
    } else if (event->value_type == EV_VALUE_TYPE_DECIMAL) {
        int d_val = fabs(strtod(value, NULL)); // TODO: use floating point
        return handle_comparison_decimal(d_val, event);
    } else {
        return -1;
    }
}

void handle_event_listeners(const struct mosquitto_message *msg, struct mqtt_sub_config *conf) {

    int rc;

    // loop over every listener, for every matching topic, get key, check if json works, etc
    for (int i = 0; i < MQTT_MAX_EVENT_LISTENER_COUNT; i++) {
        if (conf->events[i]->value_type == EV_VALUE_TYPE_UNSET) { // null check
            continue;
        }

        // if ev listener topic matches..
        if (strcmp(conf->events[i]->topic_name, msg->topic) == 0) {
            const char *output; 

            // try to parse json
            rc = event_try_parse(msg, conf->events[i]->json_key, &output);
            
            if (rc != 0) {// if failed to parse json, noop
                // TODO: possibly add log statements here? unsure.
                continue;
            }

            // we have value, we have good conf, we have good key.
            // do comparison check here. (returns rc)
            // if rc is good, send SMTP (as configured) via libcurl
            if (handle_comparison(output, conf->events[i]) == 1) {
                syslog(LOG_INFO, "Event listener with comment '%s' triggered. Sending message.", conf->events[i]->comment);
                rc = send_smtp_message(output, &(conf->emailgroup), conf->events[i]);
                if (rc == 0) {
                    syslog(LOG_INFO, "Message sent successfully.");
                } else {
                    syslog(LOG_ERR, "Failed to send SMTP message: rc: %d", rc);
                }
            }
        }
    }
}

// returns:
// 0 if successful retreival of key
// -1 if failed to parse
// -2 if failed to find value from key
static int event_try_parse(const struct mosquitto_message *msg, char *key, const char **output) {
    struct json_object *jobj;

    jobj = json_tokener_parse((char *)msg->payload);

    if (jobj == NULL) {
        return -1;
    }

    struct json_object *tmp;
    json_object_object_get_ex(jobj, key, &tmp);

    if (tmp == NULL) {
        return -2;
    }
    
    *output = json_object_get_string(tmp);
    if (*output == NULL) {
        return -2;
    }

    return 0;
}