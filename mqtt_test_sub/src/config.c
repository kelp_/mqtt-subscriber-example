#include <sys/syslog.h>
#include <syslog.h>
#include <uci.h>
#include <ucimap.h>
#include <uci_config.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#include "config.h"

struct mqtt_sub_config alloc_mqtt_sub_config() {
    struct mqtt_sub_config conf;
    size_t topic_size = sizeof(struct mqtt_conf_topic);
    size_t event_size = sizeof(struct mqtt_topic_event);
    conf.enable = 0;
    conf.port = 0;
    //conf.useTLS = 0;

    memset(conf.url, 0, MAX_URL_SIZE);

    for (int i = 0; i < MQTT_MAX_TOPIC_COUNT; i++) {
        struct mqtt_conf_topic *topic = malloc(topic_size);

        if (!topic) {
            printf("Out of memory!\n");
            syslog(LOG_CRIT, "Out of memory!");
            exit(1);
        }

        memset(topic->name, 0, MQTT_MAX_TOPIC_LEN);
 
        conf.topics[i] = topic;
    }

    for (int i = 0; i < MQTT_MAX_EVENT_LISTENER_COUNT; i++) {
        struct mqtt_topic_event *event = malloc(event_size);
        if (!event) {
            printf("Out of memory!\n");
            syslog(LOG_CRIT, "Out of memory!");
            exit(1);
        }

        memset(event->topic_name, 0, MQTT_MAX_TOPIC_LEN);
        memset(event->json_key, 0, sizeof(event->json_key));
        memset(event->str_val, 0, sizeof(event->str_val));
        memset(event->comment, 0, sizeof(event->comment));
        event->value_type = EV_VALUE_TYPE_UNSET;
        event->comp_type = EV_COMP_TYPE_UNSET;
        event->int_val = 0;

        conf.events[i] = event;
    }

    return conf;
}

void free_mqtt_sub_config(struct mqtt_sub_config conf) {
    for (int i = 0; i < MQTT_MAX_TOPIC_COUNT; i++) {
        free(conf.topics[i]);
    }

    for (int i = 0; i < MQTT_MAX_EVENT_LISTENER_COUNT; i++) {
        free(conf.events[i]);
    }
}

void debug_print_conf(struct mqtt_sub_config conf) {
    printf("mqtt_sub_config: \n-----------\n");
    printf("enable: %d\nuseTLS: N/A\nurl: %s\nport: %d\n",
        conf.enable,
        //conf.useTLS,
        conf.url,
        conf.port
    );

    printf("emailinfo: %s, sender_email: %s, smtp_url: %s, smtp_port: %d, sec_conn: %d, use_creds: %d, username: %s, pwd: %s\n", 
        conf.emailgroup.name,
        conf.emailgroup.sender_email,
        conf.emailgroup.smtp_ip,
        conf.emailgroup.smtp_port,
        conf.emailgroup.secure_connection,
        conf.emailgroup.use_credentials,
        conf.emailgroup.username,
        conf.emailgroup.password
    );

    printf("\nsub topics: \n-----------\n");
    for (int i = 0; i < MQTT_MAX_TOPIC_COUNT; i++) {
        if (strcmp(conf.topics[i]->name, "") != 0) {
            printf(" %s\n", conf.topics[i]->name);
        }
    }

    printf("\nevent listeners: \n-----------\n");
    for (int i = 0; i < MQTT_MAX_EVENT_LISTENER_COUNT; i++) {
        if (conf.events[i]->comp_type != EV_COMP_TYPE_UNSET) {
            printf("topic: %s, json_key: %s, comment: %s, value_type: %d, recipient_email: %s, comp_type: %d ",
                conf.events[i]->topic_name,
                conf.events[i]->json_key,
                conf.events[i]->comment,
                conf.events[i]->value_type,
                conf.events[i]->recipient_email,
                conf.events[i]->comp_type
            );
            // could use switch here?
            if (conf.events[i]->value_type == EV_VALUE_TYPE_DECIMAL) {
                printf("value: %d\n", conf.events[i]->int_val);
            } else if (conf.events[i]->value_type == EV_VALUE_TYPE_STRING) {
                printf("value: %s\n", conf.events[i]->str_val);
            }

        }
    }

    printf("\n");
}

// reads all email profile/group related stuff from *s, puts it into a struct.
struct event_email_group read_emailgroup(struct uci_context *ctx, struct uci_section *s) {
    struct event_email_group output;
    struct uci_option *o;

    o = uci_lookup_option(ctx, s, "name");
    if (o != NULL && o->type == UCI_TYPE_STRING) {
        output.name = o->v.string;
    } else {
        output.name = "(null)";
    }

    o = uci_lookup_option(ctx, s, "senderemail");
    if (o != NULL && o->type == UCI_TYPE_STRING) {
        output.sender_email = o->v.string;
    } else {
        output.sender_email = "(null)";
    }

    o = uci_lookup_option(ctx, s, "secure_conn");
    if (o != NULL && o->type == UCI_TYPE_STRING) {
        output.secure_connection = atoi(o->v.string);
    } else {
        output.secure_connection = -1;
    }

    o = uci_lookup_option(ctx, s, "smtp_ip");
    if (o != NULL && o->type == UCI_TYPE_STRING) {
        output.smtp_ip = o->v.string;
    } else {
        output.smtp_ip = "(null)";
    }

    o = uci_lookup_option(ctx, s, "smtp_port");
    if (o != NULL && o->type == UCI_TYPE_STRING) {
        output.smtp_port = atoi(o->v.string);
    } else {
        output.smtp_port = 465;
    }

    o = uci_lookup_option(ctx, s, "credentials");
    if (o != NULL && o->type == UCI_TYPE_STRING) {
        output.use_credentials = atoi(o->v.string);
    } else {
        output.use_credentials = -1;
    }

    o = uci_lookup_option(ctx, s, "username");
    if (o != NULL && o->type == UCI_TYPE_STRING) {
        output.username = o->v.string;
    } else {
        output.username = "(null)";
    }

    o = uci_lookup_option(ctx, s, "password");
    if (o != NULL && o->type == UCI_TYPE_STRING) {
        output.password = o->v.string;
    } else {
        output.password = "(null)";
    }


    return output;

}

// delicious spaghetti
void read_mqtt_topic_event(int *j, struct uci_context *ctx, struct uci_section *s, struct mqtt_sub_config *conf) {

    struct uci_option *o;

    int val = (*j); // index value from main cycle, has to be passed in from scope above

    o = uci_lookup_option(ctx, s, "topic_name");
    if (o != NULL && o->type == UCI_TYPE_STRING) {
        snprintf(conf->events[val]->topic_name, MAX_URL_SIZE, "%s", o->v.string);
    } else {
        snprintf(conf->events[val]->topic_name, MAX_URL_SIZE, "(null)");
    }

    o = uci_lookup_option(ctx, s, "comment");
    if (o != NULL && o->type == UCI_TYPE_STRING) {
        snprintf(conf->events[val]->comment, sizeof(conf->events[val]->comment), "%s", o->v.string);
    } else {
        snprintf(conf->events[val]->comment, sizeof(conf->events[val]->comment), "(null)");
    }

    o = uci_lookup_option(ctx, s, "json_key");
    if (o != NULL && o->type == UCI_TYPE_STRING) {
        snprintf(conf->events[val]->json_key, MAX_URL_SIZE, "%s", o->v.string);
    } else {
        snprintf(conf->events[val]->json_key, MAX_URL_SIZE, "(null)");
    }

    o = uci_lookup_option(ctx, s, "value_type");
    if (o != NULL && o->type == UCI_TYPE_STRING) {
        if (strcmp(o->v.string, "decimal") == 0) {
            conf->events[val]->value_type = EV_VALUE_TYPE_DECIMAL;
        } else if (strcmp(o->v.string, "string") == 0) {
            conf->events[val]->value_type = EV_VALUE_TYPE_STRING;
        }
    } else {
        conf->events[val]->value_type = EV_VALUE_TYPE_UNSET;
    }

    if (conf->events[val]->value_type == EV_VALUE_TYPE_UNSET) {
        syslog(LOG_ERR, "Wrong event value type for event (topic: %s, key: %s)! Ignoring.", 
            conf->events[val]->topic_name,
            conf->events[val]->json_key
        );
        return; // dont set index, let current conf struct get overwritten.
    }

    o = uci_lookup_option(ctx, s, "comparison_type");
    if (o != NULL && o->type == UCI_TYPE_STRING) {

        // code below seems inefficient, but there's no other way to do this in C, according to stackoverflow
        // https://stackoverflow.com/questions/4014827/how-can-i-compare-strings-in-c-using-a-switch-statement
        if (strcmp(o->v.string, "==") == 0) {
            conf->events[val]->comp_type = EV_COMP_TYPE_EQUAL;
        } else if (strcmp(o->v.string, "!=") == 0) {
            conf->events[val]->comp_type = EV_COMP_TYPE_NOT_EQUAL;
        } else if (strcmp(o->v.string, ">") == 0) {
            conf->events[val]->comp_type = EV_COMP_TYPE_GREATER;
        } else if (strcmp(o->v.string, "<") == 0) {
            conf->events[val]->comp_type = EV_COMP_TYPE_LESSER;
        } else if (strcmp(o->v.string, ">=") == 0) {
            conf->events[val]->comp_type = EV_COMP_TYPE_GREATER_EQUAL;
        } else if (strcmp(o->v.string, "<=") == 0) {
            conf->events[val]->comp_type = EV_COMP_TYPE_LESSER_EQUAL;
        }
    } else {
        conf->events[val]->comp_type = EV_COMP_TYPE_UNSET;
    }

    o = uci_lookup_option(ctx, s, "value");
    if (o != NULL && o->type == UCI_TYPE_STRING) {
        if (conf->events[val]->value_type == EV_VALUE_TYPE_DECIMAL) {
            conf->events[val]->int_val = atoi(o->v.string);
        } else { // string
            snprintf(conf->events[val]->str_val, sizeof(conf->events[val]->str_val), "%s", o->v.string);
        }
    } else {
        syslog(LOG_ERR, "Value unset for event (topic: %s, key: %s)! Ignoring.");
        conf->events[val]->value_type = EV_VALUE_TYPE_UNSET; // re-set value type (main comparator for non-proper struct), just in case.
        return; // dont set index, let current conf struct get overwritten.
    }

    o = uci_lookup_option(ctx, s, "recipient_email");
    if (o != NULL && o->type == UCI_TYPE_STRING) {
        conf->events[val]->recipient_email = o->v.string;
    } else {
        conf->events[val]->recipient_email = "(null)";
    }

    (*j)++; // event listener read successful, increment
}

// returns:
// 0 on successful section return
// -1 on general error
// -2 if not found
int get_user_email_group(struct uci_context *ctx, struct uci_section **output, char *group_name) {
    struct uci_element *e; 
    struct uci_option *o;

    struct uci_package *user_groups = NULL;

    if (uci_load(ctx, "user_groups", &user_groups)) {
        syslog(LOG_CRIT, "Unable to load user_gropus config.");
        return -1;
    }

    uci_foreach_element(&user_groups->sections, e) {
        *output = uci_to_section(e);
        if (strcmp((*output)->type, "email") == 0) {
            o = uci_lookup_option(ctx, *output, "name");
            if (o != NULL && o->type == UCI_TYPE_STRING) {
                if (strcmp(o->v.string, group_name) == 0) {
                    return 0;
                } 
            } else {
                continue;
            }
        }
    }

    return -2;
}

struct mqtt_sub_config read_mqtt_sub_config() {
    struct mqtt_sub_config conf = alloc_mqtt_sub_config();
    struct uci_context *ctx = uci_init();
    struct uci_package *mqtt_sub_pkg = NULL;
    struct uci_section *s;
    struct uci_option *o;
    struct uci_element *e;

    char *buf;

    if (!ctx) {// err chk
        syslog(LOG_CRIT, "Out of memory!");
        exit(1);
    }
    
    if (uci_load(ctx, "mqtt_test_sub", &mqtt_sub_pkg)) {
        syslog(LOG_CRIT, "Unable to load mqtt_test_sub config. Aborting.");
        exit(1);
    }

    int i = 0;// index for sub_topic in conf
    int j = 0;// index for topic events in conf

    // TODO: refactor code below for less repeating lines
    uci_foreach_element(&mqtt_sub_pkg->sections, e) {
        s = uci_to_section(e);

        if (strcmp(e->name, "config") == 0) {
            o = uci_lookup_option(ctx, s, "enable");
            if (o != NULL && o->type == UCI_TYPE_STRING) {
                conf.enable = atoi(o->v.string);
            } else {
                conf.enable = 0;
            }

            /*
            o = uci_lookup_option(ctx, s, "useTLS");
            if (o != NULL && o->type == UCI_TYPE_STRING) {
                conf.useTLS = atoi(o->v.string);
            } else {
                conf.useTLS = 0;
            }*/

            o = uci_lookup_option(ctx, s, "host");
            if (o != NULL && o->type == UCI_TYPE_STRING) {
                snprintf(conf.url, MAX_URL_SIZE, "%s", o->v.string);
            } else {
                snprintf(conf.url, MAX_URL_SIZE, "test.mosquitto.org");
            }

            o = uci_lookup_option(ctx, s, "port");
            if (o != NULL && o->type == UCI_TYPE_STRING) {
                conf.port = atoi(o->v.string);
            } else {
                conf.port = 1883;
            }

            o = uci_lookup_option(ctx, s, "emailgroup");
            if (o != NULL && o->type == UCI_TYPE_STRING) {
                struct uci_section *tmp;
                int rc = get_user_email_group(ctx, &tmp, o->v.string);
                if (rc != 0) {
                    syslog(LOG_ERR, "Error while reading user_groups config. Cannot send email events. (rc: %d)", rc);
                }
                conf.emailgroup = read_emailgroup(ctx, tmp);
            } else {
                syslog(LOG_ERR, "Email group unset. Cannot send email events.");
            }

        } else if (strcmp(s->type, "topic_event") == 0) {
            read_mqtt_topic_event(&j, ctx, s, &conf);
        } else { // implied sub_topic
            o = uci_lookup_option(ctx, s, "name");
            snprintf(conf.topics[i]->name, MQTT_MAX_TOPIC_LEN, "%s", o->v.string);
            i++;
        }
    }

    syslog(LOG_INFO, "Config read successful.");

    // uncomment when debugging 
    //debug_print_conf(conf);

    return conf;
}

