#ifndef MAIL_H
#define MAIL_H

// returns:
// 0 if successful send
// -1 if libcurl error
// -2 if missing email addresses
// -3 if missing smtp address or port
// -4 if use_credentials set, but no user/pwd provided
// -5 if failed to generate message
int send_smtp_message(const char *value, struct event_email_group *emailgroup, struct mqtt_topic_event *event);

#endif /* MAIL_H */